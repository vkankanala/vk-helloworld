Hello World Application
=======================

This application has following endpoints.

Contents
--------

The mapping of the URI path space is presented in the following table:

URI path                | Resource class      | HTTP methods | Notes
----------------------- | ------------------- | ------------ | --------------------------------------------------------
**_/v1/helloworld_**    | HelloWorldResource  |  GET         |  Returns `Hello World!!!`
**_/v1/message_**       | MessageResource     |  POST        |  Adds new message to the database
**_/v1/message_**       | MessageResource     |  GET         |  Gets all the messages from database
**_/v1/message_**       | MessageResource     |  DELETE      |  Delete a message from the database
**_/v1/external_**       | ExternalResource    |  GET         |  Calls external service typicode and get the data

Running the Example
-------------------

Clone the project using git clone

>    git clone https://gitlab.com/vkankanala/vk-helloworld.git

then change directory to 

>    cd HelloWorldApplication

run the following mvn to generate the war file (HelloWorldApplication-0.0.1.war)

>    mvn clean install


install the generated war(HelloWorldApplication-0.0.1.war) in JEE application server and start then hit the endpoints (I have used JBOSS)


Testing end points using curl
-----------------------------

Replace the base URI {http://localhost:8080/HelloWorldApplication} with proper one

HelloWorld endpoint

    >   curl {http://localhost:8080/HelloWorldApplication}/v1/helloworld

Message endpoint 
    
    Adding a message to the Database
    >   curl -H "Content-Type: application/json" -X POST {http://localhost:8080/HelloWorldApplication}/v1/message -d "{\"message\":\"Test Value\"}"

    Query all the messages

    >   curl {http://localhost:8080/HelloWorldApplication}/v1/message

    Delete message from the database

    >   curl -X DELETE "{http://localhost:8080/HelloWorldApplication}/v1/message/{uuid}" 

External endpoint

    Calling an external service and get the data
    
    >   curl {http://localhost:8080/HelloWorldApplication}/v1/external
    
    

