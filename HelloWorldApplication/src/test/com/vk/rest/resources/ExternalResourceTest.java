package com.vk.rest.resources;

import static org.junit.Assert.assertEquals;
import static org.powermock.api.mockito.PowerMockito.spy;
import static org.powermock.api.mockito.PowerMockito.when;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.vk.rest.business.ServiceHelper;
import com.vk.rest.exceptions.ServiceDownException;
import com.vk.rest.resources.ExternalResource;

/**
 * This Unit class is to test all methods in ExternalResource
 * @author Vikram
 *
 */
public class ExternalResourceTest {
	@InjectMocks
	private final ExternalResource resource = spy(new ExternalResource());
	@InjectMocks
	private final ServiceHelper service = Mockito.mock(ServiceHelper.class);
	
	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testGetPosts() throws ServiceDownException {

		when(service.getPosts()).thenReturn("Success");
		
		Response res = resource.getPosts();
		assertEquals(Status.OK.getStatusCode(), res.getStatus());
	}
	
	@Test
	public void testGetPostsException() throws ServiceDownException {

		when(service.getPosts()).thenThrow(new ServiceDownException());
		
		Response res = resource.getPosts();
		assertEquals(Status.INTERNAL_SERVER_ERROR.getStatusCode(), res.getStatus());
}


}
