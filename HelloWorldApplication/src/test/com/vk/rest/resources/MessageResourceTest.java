package com.vk.rest.resources;

import static org.junit.Assert.assertEquals;
import static org.powermock.api.mockito.PowerMockito.spy;
import static org.powermock.api.mockito.PowerMockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.vk.rest.business.MessageBean;
import com.vk.rest.exceptions.InvalidaMessageException;
import com.vk.rest.mapper.MessageMapper;
import com.vk.rest.models.Message;
import com.vk.rest.representations.MessageRepresentation;
import com.vk.rest.resources.MessageResource;

/**
 * This Unit class is to test all methods in MessageResource
 * @author Vikram
 *
 */
public class MessageResourceTest {
	@InjectMocks
	private final MessageResource resource = spy(new MessageResource());
	@InjectMocks
	private final MessageBean service = Mockito.mock(MessageBean.class);
	@InjectMocks
	private final MessageMapper mapper = Mockito.mock(MessageMapper.class);
	@InjectMocks
	private final UriInfo uriInfo = Mockito.mock(UriInfo.class);

	private List<Message> messagesList;
	
	private Message mockMessage;
	
	private static final String URI_BASE_PATH = "http://localhost:8080/HelloWorldApplication/v1/";
	
	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		messagesList = mockMessageList();
		mockMessage = messagesList.get(0);
		UriBuilder uriBuilder = UriBuilder.fromPath(URI_BASE_PATH);
		when(uriInfo.getBaseUriBuilder()).thenReturn(uriBuilder);
	}

	@Test
	public void testGetAllMessages() {
		when(service.getMessages()).thenReturn(messagesList);
		
		Response res = resource.getAllMessages();
		
		assertEquals(Status.OK.getStatusCode(), res.getStatus());
	}

	@Test
	public void testGetMessage() {
		when(service.findByID(Matchers.anyString())).thenReturn(mockMessage);
		String uuid = mockMessage.getUuid();
		
		Response res = resource.getMessage(uuid);
		
		assertEquals(Status.OK.getStatusCode(), res.getStatus());
	}

	@Test
	public void testCreateMessage() throws InvalidaMessageException {
		when(service.createMessage(Matchers.any(Message.class))).thenReturn(mockMessage);
		MessageRepresentation msgRep = new MessageRepresentation();
		msgRep.setMessage(mockMessage.getMessage());
		
		Response res = resource.createMessage(msgRep);
		
		assertEquals(Status.CREATED.getStatusCode(), res.getStatus());
	}

	@Test
	public void testDeleteEmployee() {
		when(service.findByID(Matchers.anyString())).thenReturn(mockMessage);
		String uuid = mockMessage.getUuid();
		
		Response res = resource.deleteEmployee(uuid);
		
		assertEquals(Status.ACCEPTED.getStatusCode(), res.getStatus());
	}

	private List<Message> mockMessageList() {
		List<Message> msgList = new ArrayList<Message>();
		for(int i = 0; i < 5; i++) {
			Message msg = new Message();
			msg.setId(i);
			msg.setMessage("Hello World!!!" + i);
			msg.setUuid(UUID.randomUUID().toString());
			
			msgList.add(msg);
		}
		return msgList;
		
	}
}
