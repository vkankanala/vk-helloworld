package com.vk.rest.resources;

import javax.ws.rs.core.Response;

import static org.junit.Assert.*;
import org.junit.Test;

import com.vk.rest.resources.HelloWorldResource;

/**
 * This Unit test class is to test Helloworld resource methods
 * 
 * @author Vikram
 *
 */
public class HelloWorldResourceTest {
	
    @Test
    public void get() throws Exception {
    	HelloWorldResource helloWorld = new HelloWorldResource();
    	
    	Response res = helloWorld.get();
    	
    	assertEquals(HelloWorldResource.HELLO_WORLD, (String)res.getEntity());
    }
}
