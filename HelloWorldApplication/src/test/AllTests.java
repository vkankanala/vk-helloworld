import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.vk.rest.business.MessageBeanTest;
import com.vk.rest.data.MessageDAOTest;
import com.vk.rest.mapper.MessageMapperTest;
import com.vk.rest.resources.ExternalResourceTest;
import com.vk.rest.resources.HelloWorldResourceTest;
import com.vk.rest.resources.MessageResourceTest;

@RunWith(Suite.class)
@SuiteClasses({MessageBeanTest.class, 
	MessageDAOTest.class, 
	MessageMapperTest.class, 
	ExternalResourceTest.class, 
	HelloWorldResourceTest.class, 
	MessageResourceTest.class})
public class AllTests {

}
