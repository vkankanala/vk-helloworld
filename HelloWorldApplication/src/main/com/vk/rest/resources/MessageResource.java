package com.vk.rest.resources;

import java.net.URI;
import java.util.List;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.vk.rest.business.MessageBean;
import com.vk.rest.exceptions.NoSuchMessageException;
import com.vk.rest.mapper.MessageMapper;
import com.vk.rest.representations.MessageRepresentation;
/**
 * This Resource end point supports http GET, POST and DELETE operations
 * 
 * GET - Retrieves all the records from the database
 * POST- Adds a new message record into the database
 * DELETE - Deletes a message record from the database for the given message id
 * 
 * @author Vikram
 *
 */
@Path("/message")
public class MessageResource {
	@Context
	UriInfo uriInfo;

	@EJB
	private MessageBean msgBean;
	@Inject
	private MessageMapper msgMapper;
	/**
	 * GET http operation for MessageResource Resource to query all message rows from the Database
	 * @return
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllMessages() {
		List<com.vk.rest.models.Message> msgList = msgBean.getMessages();
		return Response.ok(msgMapper.mapMessages(msgList, uriInfo)).build();
	}
	/**
	 * GET http operation for MessageResource Resource to get a particular message from the Database
	 * 
	 * @param msgId
	 * @return
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{msg-id}")
	public Response getMessage(@PathParam("msg-id") String msgId) {
		com.vk.rest.models.Message msg = msgBean.findByID(msgId);
		if(msg == null) {
			Response.status(200).entity("MessageResource not found").build();
		}
		
		return Response.ok(msgMapper.mapMessage(msg, uriInfo)).build();
	}
	/**
	 * POST http operation for MessageResource to add a new message record in the database
	 * @param messageRepresentation
	 * @return
	 */
	@POST
    @Consumes(MediaType.APPLICATION_JSON)
	public Response createMessage(MessageRepresentation messageRepresentation) {
		com.vk.rest.models.Message msg = null;
		
		try {
			msg = msgBean.createMessage(msgMapper.mapMessageRepresentation(messageRepresentation));
		}catch(Exception e) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		
		URI createdURI = uriInfo.getBaseUriBuilder().path(MessageResource.class).path(MessageResource.class, "getMessage").build(msg.getUuid());
		
		return Response.created(createdURI).build();
	}
	
	/**
	 * DELETE http operation for Message Resource to delete a message record from database
	 * @param msg_id
	 * @return
	 */
	@DELETE
    @Path("/{msg_id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteEmployee(@PathParam("msg_id") String msg_id){
		try {
			msgBean.deleteMessage(msg_id);
		} catch(NoSuchMessageException e){
            return Response.noContent().entity("Message not found").build();
        }
        return Response.accepted().entity("Message is deleted").build();
    }	
}
