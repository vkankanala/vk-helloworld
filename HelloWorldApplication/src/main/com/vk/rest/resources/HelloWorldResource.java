/**************************************************************************
 * 
 * File Name: HelloWorldResource.java
 * 
 **************************************************************************/
package com.vk.rest.resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Hello World Resource supports http GET operation
 * 
 * @author Vikram
 *
 */
@Path("/helloworld")
public class HelloWorldResource {
	
	public static final String HELLO_WORLD = "Hello World!!!";

	/**
	 * GET http operation for HelloWorldResource 
	 * @return
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response get() {
		return Response.ok().entity(HELLO_WORLD).build();
	}

}
