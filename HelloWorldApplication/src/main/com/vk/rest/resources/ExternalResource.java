package com.vk.rest.resources;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.vk.rest.business.ServiceHelper;
import com.vk.rest.exceptions.ServiceDownException;

/**
 * This Resource supports http GET and calls the external service GET and returns the response
 * 
 * @author Vikram
 *
 */

@Path("/external")
public class ExternalResource {
	@Inject
	ServiceHelper service;
	
	/**
	 * GET method to retrieve posts from typicode service
	 * @return
	 */
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getPosts() {
		try{
			//Get posts from external typicode service
			String posts = service.getPosts();
			
			return Response.ok().entity(posts).build();
		} catch(ServiceDownException e) {
			return Response.serverError().build();
		}
	}

}
