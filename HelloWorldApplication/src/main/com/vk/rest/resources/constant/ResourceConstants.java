package com.vk.rest.resources.constant;

public class ResourceConstants {
	
	/**
	 * String for typeicode end point url
	 */
	public static final String TYPICODE_URL = "https://jsonplaceholder.typicode.com";
	/**
	 * String for typeicode resource name
	 */
	public static final String TYPICODE_RESOURCE = "posts";

}
