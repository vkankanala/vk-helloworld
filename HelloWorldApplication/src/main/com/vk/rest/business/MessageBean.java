package com.vk.rest.business;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.vk.rest.data.MessageDAO;
import com.vk.rest.exceptions.InvalidaMessageException;
import com.vk.rest.exceptions.NoSuchMessageException;
import com.vk.rest.models.Message;
/**
 * This is business service bean to apply any business logic before calling DAO
 * 
 * @author Vikram
 *
 */
@Stateless
public class MessageBean {
	private static Logger LOGGER = Logger.getLogger(MessageBean.class.getName());
	@Inject
	MessageDAO messageDAO;

	/**
	 * Retrieves all the messages
	 * @return
	 */
	public List<Message> getMessages(){
		return messageDAO.getMessages();
	
	}
	
	/**
	 * Creates a new message
	 * @param msg
	 * @return
	 */
	public Message  createMessage(String msg) throws InvalidaMessageException{
		if(msg == null || msg.isEmpty()) {
			LOGGER.log(Level.INFO, "Attempted to create message but message text is empty");
			throw new InvalidaMessageException();
		}
		Message message = new Message();
		message.setMessage(msg);
	
		return messageDAO.createMessage(message);
				
	}
	/**
	 * Creates message
	 * @param msg
	 * @return
	 */
	public Message  createMessage(Message msg) {
		
		return  messageDAO.createMessage(msg);
	}
	/**
	 * Finds the message by the given uuid
	 * 
	 * @param uuid
	 * @return
	 */
	public Message findByID(String uuid) {
		return  messageDAO.findByID(uuid);
		
	}
	/**
	 * Deletes the message for the given uuid
	 * @param uuid
	 * @return
	 */
	public void deleteMessage(String uuid) throws NoSuchMessageException{
		try {
			messageDAO.deleteMessage(uuid);
		}catch(NoSuchMessageException e) {
			LOGGER.log(Level.INFO, "Attempted to delete message: {0} but it doesn't exist", new Object[] {uuid});
			throw e;
		}
	}

}
