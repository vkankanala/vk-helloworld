package com.vk.rest.business;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.vk.rest.exceptions.ServiceDownException;
import com.vk.rest.resources.constant.ResourceConstants;
/**
 * Service helper to call the typicode resource end point
 * @author Vikram
 *
 */
public class ServiceHelper {
	private static Logger LOGGER = Logger.getLogger(ServiceHelper.class.getName());
	
	/**
	 * Calls the typicode posts resource and returns the response
	 * @return
	 */
	public String getPosts() throws ServiceDownException {
		try {
			Client client = ClientBuilder.newClient();
			WebTarget webTarget = client.target(ResourceConstants.TYPICODE_URL).path(ResourceConstants.TYPICODE_RESOURCE);
			 
			Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON).header("user-agent", "");
			Response response = invocationBuilder.get(Response.class);
			     
	
	        LOGGER.log(Level.INFO, "Status: " + response.getStatus());
	        LOGGER.log(Level.INFO, "StatusInfo:", response.getStatusInfo());
			String posts = response.readEntity(String.class);
			LOGGER.log(Level.INFO, "readEntity(String): " + posts);
			
	        return posts;
		} catch(Exception e) {
			LOGGER.log(Level.INFO, "external service call failed with exception " + e.getMessage());
			throw new ServiceDownException();
		}
	}
}
