package com.vk.rest.data;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.vk.rest.exceptions.NoSuchMessageException;
import com.vk.rest.models.Message;
/**
 * Data access class provides all data access methods
 * 
 * @author Vikram
 *
 */
public class MessageDAO {
	@PersistenceContext(unitName="HelloWorldModel")
	private EntityManager em;

	/**
	 * Retrieves all the messages
	 * @return
	 */
	public List<Message> getMessages(){
		TypedQuery<Message> query = em.createQuery("SELECT m FROM Message m", Message.class);
		List<Message> results = query.getResultList();

		return results;
	
	}
	
	/**
	 * Creates a new message
	 * @param msg
	 * @return
	 */
	public Message  createMessage(Message msg) {
		em.persist(msg);
		
		return msg;
	}
	/**
	 * Finds the message by the given uuid
	 * 
	 * @param uuid
	 * @return
	 */
	public Message findByID(String uuid) {
		Query query = em.createQuery("select m from Message m where m.uuid = :uuid").setParameter("uuid", uuid);
		return (Message)query.getSingleResult();
		
	}
	/**
	 * Deletes the message for the given uuid
	 * 
	 * @param uuid
	 * @return
	 */
	public void deleteMessage(String uuid) throws NoSuchMessageException{
		try {
//		int deleteRecords = em.createQuery("delete from Message m where m.uuid = :uuid")
//				.setParameter("uuid", uuid)
//                .executeUpdate();
//		return deleteRecords;
			Message msg = findByID(uuid);
			em.remove(msg);
		}catch(Exception e) {
			throw new NoSuchMessageException();
		}
	}

}
