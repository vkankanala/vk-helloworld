package com.vk.rest.mapper;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Link;
import javax.ws.rs.core.UriInfo;

import com.vk.rest.models.Message;
import com.vk.rest.representations.MessageRepresentation;

public class MessageMapper {
	
	/**
	 * Maps MessageRepresentation Object to MessageResource Object
	 * 
	 * @param messageRepresentation
	 * @return
	 */
	public Message mapMessageRepresentation(MessageRepresentation messageRepresentation) {
		Message msg = new Message();
		if(messageRepresentation.getUuid() != null && !messageRepresentation.getUuid().isEmpty()) {
			msg.setUuid(messageRepresentation.getUuid());
		}
		msg.setMessage(messageRepresentation.getMessage());
		
		return msg;
		
	}
	
	/**
	 * Maps MessageResource Object to MessageRepresentation Object
	 * 
	 * @param message
	 * @return
	 */
	public MessageRepresentation mapMessage(Message message, UriInfo uriInfo) {
		MessageRepresentation msgRepresentation = new MessageRepresentation();
		msgRepresentation.setUuid(message.getUuid());
		msgRepresentation.setMessage(message.getMessage());
		//build self link
	     Link selfLink = Link.fromUri(uriInfo.getPath() + "/" + message.getUuid()).rel("self").build();
	     msgRepresentation.getLinks().add(selfLink);
		
		return msgRepresentation;
		
	}
	
	/**
	 * Maps List of MessageRepresentation objects to MessageResource Objects
	 * 
	 * @param msgRepList
	 * @return
	 */
	public List<Message> mapMessageRepresentations(List<MessageRepresentation> msgRepList) {
		List<Message> msgList = new ArrayList<Message>();
		
		for(MessageRepresentation msgRep : msgRepList) {
			msgList.add(mapMessageRepresentation(msgRep));
		}
		
		return msgList;
	}

	/**
	 * Maps List of MessageResource objects to MessageRepresentation Objects
	 * 
	 * @param msgList
	 * @return
	 */
	public List<MessageRepresentation> mapMessages(List<Message> msgList, UriInfo uriInfo) {
		List<MessageRepresentation> msgRepList = new ArrayList<MessageRepresentation>();
		
		for(Message msg : msgList) {
			msgRepList.add(mapMessage(msg, uriInfo));
		}
		
		return msgRepList;
	}
}
