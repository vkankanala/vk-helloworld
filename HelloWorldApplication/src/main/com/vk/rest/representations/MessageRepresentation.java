package com.vk.rest.representations;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Link;

/**
 * 
 * @author bij1236
 *
 */
public class MessageRepresentation {
	private List<Link> links = new ArrayList<Link>();
	
	private String message;
	
	private String uuid;

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	
	

}
