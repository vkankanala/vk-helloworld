/**
 * 
 */
package com.vk.rest;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import com.vk.rest.resources.ExternalResource;
import com.vk.rest.resources.HelloWorldResource;
import com.vk.rest.resources.MessageResource;

/**
 * Hello World Application will have all the hello world resources
 * @author Vikram
 *
 */
@ApplicationPath("/v1")
public class HelloWorldApplication extends Application {

	@Override
	public Set<Class<?>> getClasses() {
		Set<Class<?>> classes = new HashSet<Class<?>>();
		classes.add(HelloWorldResource.class);
		classes.add(MessageResource.class);
		classes.add(ExternalResource.class);
		
		return super.getClasses();
	}

}
